import numpy as np
import requests
import os

def getAll(url):
    next = url
    data = []
    while next:
        res = requests.get(next)
        res = res.json()
        next = res['next']
        for item in res['results']:
            data.append(item)
    return data
def getPlanetsUrlByWheather(url,clima):
    planets = getAll(url)
    data = []
    for item in planets:
        if clima in item['climate']:
            data.append(item['url'])
    return data


def planetasAridos(url,planets):
    planets = np.asarray(planets)
    peliculas = getAll(url)
    data = []
    for item in peliculas: 
        planet = np.asarray(item['planets'])
        if np.any(np.in1d(planets, planet)):
            data.append(item)
    return data

def searchPlanetsClimate(climate):
    planets = getAll('https://swapi.dev/api/planets/')
    aridPlanets = []
    for item in planets:
        if item['climate'] == climate:
            aridPlanets.append(item)
    return aridPlanets


def getUrlSpecie(name):
    res = requests.get('https://swapi.dev/api/species/?search='+name)
    res = res.json()
    data = res['results']
    return data[0]['url']

def getChatactersInFilms(film):
    res = requests.get(film)
    res = res.json()
    data = res
    return data['characters']

def whatAreYou(people):
    res = requests.get(people)
    res = res.json()
    data = res['species']
    return data

def countSpeciesInFilms(films,species):
    req = requests.get(films)
    req = req.json()
    peliculaPersonajes = req['characters']
    res = []
    for item in peliculaPersonajes:
        reqNameSpecie = requests.get(item).json()
        nameSpecie = reqNameSpecie['species']
        try:
            if species in nameSpecie:
                res.append(item)
        except:
            pass
        
    return res

def fastConutSpeciesInFilms(urlFilms,urlSpecies):
    filmsCharacter = getChatactersInFilms(urlFilms)
    wally = urlSpecies
    res = []
    for item in filmsCharacter:
        iam = whatAreYou(item)
        try:
            if wally == iam[0]:
                res.append(item)
        except:
            pass
        
    return res
    
def getBigWave(url):
    waves = getAll(url)
    bigWave = waves[0]
    for item in waves:
        if float(item['length'].replace(',', '')) > float(bigWave['length'].replace(',', '')):
            bigWave = item
    return bigWave



if __name__ == "__main__":
    print('\n'+"""
     _______.____    __    ____  ___      .______    __  
    /       |\   \  /  \  /   / /   \     |   _  \  |  | 
   |   (----` \   \/    \/   / /  ^  \    |  |_)  | |  | 
    \   \      \            / /  /_\  \   |   ___/  |  | 
.----)   |      \    /\    / /  _____  \  |  |      |  | 
|_______/        \__/  \__/ /__/     \__\ | _|      |__| 
                                                         
     """+'\n')

    print('Películas en la que aparecen planetas cuyo clima sea árido\n')

    peliculas_plAridos = planetasAridos('https://swapi.dev/api/films/',getPlanetsUrlByWheather('https://swapi.dev/api/planets/','arid'))
    
    for item in peliculas_plAridos:
        print(f'URL:{item["url"]} - Episodio:{item["episode_id"]} - Titulo:{item["title"]}\n')

    print(f'total:{len(peliculas_plAridos)}\n')

    print('Wookies que aparecen en la sexta película\n')

    especie_aparicion = countSpeciesInFilms('http://swapi.dev/api/films/6/','http://swapi.dev/api/species/3/')
    
    for item in especie_aparicion:
        res = requests.get(item).json()
        print(f'Name:{res["name"]} - Genero:{res["gender"]} - url:{res["url"]}\n')
    print(f'total:{len(especie_aparicion)}\n')

    print('3 -La nave mas grande\n')
    print(getBigWave('https://swapi.dev/api/starships/'))
    os.system("pause")